# Code Challenge #
------------------

# Code supplied layout #

* Code out RESULT.png as index.php using HTML, CSS, and image resources
~~~~
    • Font Color: #555555
    • Blue Font Color: #0665B1
    • Font Family: Open Sans
    • General Font Size: 15pt
    • Heading Font Size: 25pt
~~~~

* When hovering over left hand menu items, turn text blue

* Include the "main_content.php" file for the text in the main content block.

* Below "Lorem ipsum..." content, use PHP to display the numbers 1 to 1000 with a line break after each number

###Notes:###
* We're not worried about responsiveness, so text can wrap however you prefer.
* All resources linked relatively (i.e. src="resources/filename.ext")
* Don't stress about spacing (padding/margin) - we're looking for general skill, not pixel-perfection
------------------

# Evaluate Supplied PHP File #
Tell us what the review.php file does with in-line comments (assuming a successful mysql_connect connection).  Please note **anything** that might be wrong with the written code without making corrections.